/***********************************************************************************************//**
 * \file   main.c
 * \brief  Silicon Labs Empty Example Project
 *
 * This example demonstrates the bare minimum needed for a Blue Gecko C application
 * that allows Over-the-Air Device Firmware Upgrading (OTA DFU). The application
 * starts advertising after boot and restarts advertising after a connection is closed.
 ***************************************************************************************************
 * <b> (C) Copyright 2016 Silicon Labs, http://www.silabs.com</b>
 ***************************************************************************************************
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 **************************************************************************************************/

/* Board headers */
#include "boards.h"
#include "ble-configuration.h"
#include "board_features.h"

/* Bluetooth stack headers */
#include "bg_types.h"
#include "native_gecko.h"
#include "gatt_db.h"
#include "aat.h"

/* Libraries containing default Gecko configuration values */
#include "em_emu.h"
#include "em_cmu.h"
#ifdef FEATURE_BOARD_DETECTED
#include "bspconfig.h"
#include "pti.h"
#endif

/* Device initialization header */
#include "InitDevice.h"

#ifdef FEATURE_SPI_FLASH
#include "em_usart.h"
#include "mx25flash_spi.h"
#endif /* FEATURE_SPI_FLASH */

/***********************************************************************************************//**
 * @addtogroup Application
 * @{
 **************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>

/***********************************************************************************************//**
 * @addtogroup app
 * @{
 **************************************************************************************************/
void readPasskey(void);
void pressKey(uint8_t);
void releaseKey(uint8_t);

uint8_t report[8];

#ifndef MAX_CONNECTIONS
#define MAX_CONNECTIONS 4
#endif
uint8_t bluetooth_stack_heap[DEFAULT_BLUETOOTH_HEAP(MAX_CONNECTIONS)];

#ifdef FEATURE_PTI_SUPPORT
static const RADIO_PTIInit_t ptiInit = RADIO_PTI_INIT;
#endif

/* Gecko configuration parameters (see gecko_configuration.h) */
static const gecko_configuration_t config = {
		.config_flags = 0,
		.sleep.flags = SLEEP_FLAGS_DEEP_SLEEP_ENABLE,
		.bluetooth.max_connections = MAX_CONNECTIONS,
		.bluetooth.heap = bluetooth_stack_heap,
		.bluetooth.heap_size = sizeof(bluetooth_stack_heap),
		.bluetooth.sleep_clock_accuracy = 100, // ppm
		.gattdb = &bg_gattdb_data,
		.ota.flags = 0,
		.ota.device_name_len = 3,
		.ota.device_name_ptr = "OTA",
#ifdef FEATURE_PTI_SUPPORT
		.pti = &ptiInit,
#endif
};

/* Flag for indicating DFU Reset must be performed */
uint8_t boot_to_dfu = 0;
uint8_t activeConnID = 0;
uint8_t needsToReadPasskey = 0;

uint8_t bonded = 0;
uint8_t periodicSoftTimerID = 0;

/**
 * @brief  Main function
 */
void main(void)
{
#ifdef FEATURE_SPI_FLASH
	/* Put the SPI flash into Deep Power Down mode for those radio boards where it is available */
	MX25_init();
	MX25_DP();
	/* We must disable SPI communication */
	USART_Reset(USART1);

#endif /* FEATURE_SPI_FLASH */

	/* Initialize peripherals */
	enter_DefaultMode_from_RESET();

	/* Initialize stack */
	gecko_init(&config);

	RETARGET_SerialInit();

	report[0] = 0;
	report[1] = 0;
	report[2] = 0;
	report[3] = 0;
	report[4] = 0;
	report[5] = 0;
	report[6] = 0;
	report[7] = 0;

	while (1) {
		/* Event pointer for handling events */
		struct gecko_cmd_packet* evt;

		/* Check for stack event. */
		evt = gecko_wait_event();

		/* Handle events */
		switch (BGLIB_MSG_ID(evt->header)) {
			/* This boot event is generated when the system boots up after reset.
			 * Here the system is set to start advertising immediately after boot procedure. */
			case gecko_evt_system_boot_id:
				printf("System booted\r\n");

				// Simplify testing
				gecko_cmd_sm_delete_bondings();
				gecko_cmd_sm_configure(1, sm_io_capability_displayonly );

				// Enable bondable to accomodate certain mobile OS
				gecko_cmd_sm_set_bondable_mode(1);

				/* Set advertising parameters. 100ms advertisement interval. All channels used.
				 * The first two parameters are minimum and maximum advertising interval, both in
				 * units of (milliseconds * 1.6). The third parameter '7' sets advertising on all channels. */
				gecko_cmd_le_gap_set_adv_parameters(160, 160, 7);

				/* Start general advertising and enable connections. */

				gecko_cmd_le_gap_set_mode(le_gap_general_discoverable, le_gap_undirected_connectable);
				break;
			case gecko_evt_le_connection_opened_id:
				printf("Connection opened\r\n");

				// Store connection ID
				activeConnID = evt->data.evt_le_connection_opened.connection;

				// Increase security level to trigger pairing
				gecko_cmd_sm_increase_security(activeConnID);
				printf("Connected to conn: %u\r\n", evt->data.evt_le_connection_opened.connection);
				printf("Bonding: %u\r\n", evt->data.evt_le_connection_opened.bonding);
				printf("Device is master: %u\r\n", evt->data.evt_le_connection_opened.master);
				break;
			case gecko_evt_sm_passkey_request_id:
				// Not used
				printf("Enter passkey on conn: %u\r\n", evt->data.evt_sm_passkey_request.connection);
				needsToReadPasskey = 1;
				readPasskey();
				break;
			case gecko_evt_sm_passkey_display_id:
				printf("Passkey display\r\n");
				printf("Enter this passkey on the tablet:\r\n%d\r\n",
						evt->data.evt_sm_passkey_display.passkey);
				break;
			case gecko_evt_le_connection_closed_id:
				printf("Connection closed\r\n");
				/* Check if need to boot to dfu mode */
				if (boot_to_dfu) {
					/* Enter to DFU OTA mode */
					gecko_cmd_system_reset(2);
				} else {
					/* Restart advertising after client has disconnected */
					gecko_cmd_le_gap_set_mode(le_gap_general_discoverable, le_gap_undirected_connectable);
				}
				break;
				/* This event is triggered after the bonding procedure has been succesfully completed. */
			case gecko_evt_sm_bonded_id:
				bonded = 1;

				// start a sw timer for every 1s
				gecko_cmd_hardware_set_soft_timer(32768, 0, 0);
				break;

			case  gecko_evt_sm_bonding_failed_id:
				bonded = 0;
				break;

				/* This event indicates that a remote GATT client is attempting to read a value of an
				 *  attribute from the local GATT database, where the attribute was defined in the GATT
				 *  XML firmware configuration file to have type="user". */
			case gecko_evt_gatt_server_user_read_request_id:

				break;

				/* This event indicates that a remote GATT client is attempting to write a value of an
				 * attribute in to the local GATT database, where the attribute was defined in the GATT
				 * XML firmware configuration file to have type="user".  */
			case gecko_evt_gatt_server_user_write_request_id:

				break;

				// Software timer handling
			case gecko_evt_hardware_soft_timer_id:
				// send a keyboard report
				pressKey('A');
				releaseKey('A');
				break;
			default:
				break;
		}
	}
}

/** @} (end addtogroup app) */

// not used
void readPasskey(void) {
	static char passkey_char[7];
	static char input;
	static uint32 passkey_uint32;
	static int  keyCnt = 0;

	if (needsToReadPasskey) {
		printf("Reading passkey\r\n");
		input = getchar();
		printf("%u\r\n", input);
		if(input != '\0' && input != 0xFF ) /* Filter */
		{
			passkey_char[keyCnt] = input; 	/* Store each digit as a string */
			keyCnt++;

			if (keyCnt == 6)
			{
				printf("Done reading\r\n");
				needsToReadPasskey = 0;
				passkey_char[keyCnt] = '\0'; /* Add the string terminator */
				/* Convert the pin string to uint32 */
				passkey_uint32 = atoi(passkey_char);
				/* Push the pin number to the stack */
				gecko_cmd_sm_enter_passkey(activeConnID, passkey_uint32);
			}
		}
	}
}

void pressKey(uint8_t character) {
	report[2] = character;
	gecko_cmd_gatt_server_send_characteristic_notification(
			activeConnID,
			gattdb_boot_keyboard_input_report,
			8,
			report
	);
}
void releaseKey(uint8_t character) {
	report[2] = 0;
	gecko_cmd_gatt_server_send_characteristic_notification(
			activeConnID,
			gattdb_boot_keyboard_input_report,
			8,
			report
	);
}
/** @} (end addtogroup Application) */
